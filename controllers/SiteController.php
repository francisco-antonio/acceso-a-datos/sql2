<?php

namespace app\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;
use app\models\Ciclista;
use app\models\Lleva;
use app\models\Puerto;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionConsulta1a(){
        //Mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find() ->select("COUNT(*) num_ciclistas")->distinct(),
            'pagination'=>[
                    'pageSize'=>0,
                ]
            ]);
        
        return $this->render("resultados",[
            "resultado"=>$dataProvider,
            "campos"=>['num_ciclistas'],
            "titulo"=>"Consulta 1 con Active Record",
            "enunciado"=>"Número de ciclistas que hay",
            "sql"=>"SELECT COUNT(*) num_ciclistas;",
            
        ]);
    }
      /*CONSULTAS CON DAO*/
public function actionConsulta1(){

$numero = Yii::$app->db
	->createCommand('SELECT COUNT(*) num_ciclistas FROM ciclista;')
	->queryScalar();


$dataProvider = new SqlDataProvider([

'sql'=>'SELECT COUNT(*) num_ciclistas FROM ciclista;',

'totalCount'=>$numero,
'pagination'=>[
'pageSize' => 0,
]
]);


    return $this->render ("resultados",[

        "resultado"=>$dataProvider,

        "campos"=>['num_ciclistas'],

        "titulo"=>"Consulta 1 con DAO",

        "enunciado"=>"Listar las edades de los ciclistas (sin repetir)",

        "sql"=>"SELECT DISTINCT edad FROM ciclistas",

]);

}
    public function actionConsulta2a(){
        //Mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find() ->select("COUNT(*) num_ciclistas_Banesto")->distinct()->where("nomequipo='Banesto'"),
            'pagination'=>[
                    'pageSize'=>5,
                ]
            ]);
        
        return $this->render("resultados",[
            "resultado"=>$dataProvider,
            "campos"=>['num_ciclistas_Banesto'],
            "titulo"=>"Consulta 2 con Active Record",
            "enunciado"=>"Número de ciclistas que hay del equipo Banesto",
            "sql"=>"SELECT COUNT(*) FROM ciclista c WHERE c.nomequipo='Banesto';",
            
        ]);
    }
         /*CONSULTAS CON DAO*/
public function actionConsulta2(){

$numero = Yii::$app->db
	->createCommand("SELECT COUNT(*) num_ciclistas FROM ciclista where nomequipo='Banesto';")
	->queryScalar();


$dataProvider = new SqlDataProvider([

'sql'=>"SELECT COUNT(*) num_ciclistas_Banesto FROM ciclista where nomequipo='Banesto';",

'totalCount'=>$numero,
'pagination'=>[
'pageSize' => 5,
]
]);


    return $this->render ("resultados",[

        "resultado"=>$dataProvider,

        "campos"=>['num_ciclistas_Banesto'],

        "titulo"=>"Consulta 2 con DAO",

        "enunciado"=>"Número de ciclistas que hay del equipo Banesto",

        "sql"=>"SELECT COUNT(*) num_ciclistas_Banesto FROM ciclista where nomequipo='Banesto';",

]);

}
    public function actionConsulta3a(){
        //Mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find() ->select("AVG(edad) media_Edad")->distinct(),
            'pagination'=>[
                    'pageSize'=>5,
                ]
            ]);
        
        return $this->render("resultados",[
            "resultado"=>$dataProvider,
            "campos"=>['media_Edad'],
            "titulo"=>"Consulta 3 con Active Record",
            "enunciado"=>"Edad media de los ciclistas",
            "sql"=>"SELECT AVG(edad) media_Edad FROM ciclista ;",
            
        ]);
    }
          /*CONSULTAS CON DAO*/
public function actionConsulta3(){

$numero = Yii::$app->db
	->createCommand("SELECT AVG(edad) media_Edad FROM ciclista ;")
	->queryScalar();


$dataProvider = new SqlDataProvider([

'sql'=>"SELECT AVG(edad) media_Edad FROM ciclista ;",

'totalCount'=>$numero,
'pagination'=>[
'pageSize' => 5,
]
]);


    return $this->render ("resultados",[

        "resultado"=>$dataProvider,

        "campos"=>['media_Edad'],

        "titulo"=>"Consulta 3 con DAO",

        "enunciado"=>"Edad media de los ciclistas",

        "sql"=>"SELECT AVG(edad) media_Edad FROM ciclista ;",

]);

}
    public function actionConsulta4a(){
        //Mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find() ->select("AVG(edad) media_Edad")->distinct()->where("nomequipo='Banesto'"),
            'pagination'=>[
                    'pageSize'=>5,
                ]
            ]);
        
        return $this->render("resultados",[
            "resultado"=>$dataProvider,
            "campos"=>['media_Edad'],
            "titulo"=>"Consulta 4 con Active Record",
            "enunciado"=>"Edad media de los ciclistas de Banesto",
            "sql"=>"SELECT AVG(edad) media_Edad FROM ciclista where nomequipo='Banesto' ;",
            
        ]);
    }
     /*CONSULTAS CON DAO*/
public function actionConsulta4(){

$numero = Yii::$app->db
	->createCommand("SELECT AVG(edad) media_Edad FROM ciclista where nomequipo='Banesto' ;")
	->queryScalar();


$dataProvider = new SqlDataProvider([

'sql'=>"SELECT AVG(edad) media_Edad FROM ciclista where nomequipo='Banesto' ;",

'totalCount'=>$numero,
'pagination'=>[
'pageSize' => 5,
]
]);


    return $this->render ("resultados",[

        "resultado"=>$dataProvider,

        "campos"=>['media_Edad'],

        "titulo"=>"Consulta 4 con DAO",

        "enunciado"=>"Edad media de los ciclistas de Banesto",

        "sql"=>"SELECT AVG(edad) media_Edad FROM ciclista where nomequipo='Banesto' ;",

]);
}
     public function actionConsulta5a(){
        //Mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find() ->select("AVG(edad)media_Edad , nomequipo")->distinct()->groupBy("nomequipo"),
            'pagination'=>[
                    'pageSize'=>5,
                ]
            ]);
        
        return $this->render("resultados",[
            "resultado"=>$dataProvider,
            "campos"=>['media_Edad'],
            "titulo"=>"Consulta 5 con Active Record",
            "enunciado"=>"La edad media de los ciclistas por cada equipo ",
            "sql"=>"SELECT AVG(edad), nomequipo FROM ciclista GROUP BY nomequipo;",
            
        ]);
    }
         /*CONSULTAS CON DAO*/
public function actionConsulta5(){

$numero = Yii::$app->db
	->createCommand("SELECT AVG(edad) FROM ciclista GROUP BY nomequipo;")
	->queryScalar();


$dataProvider = new SqlDataProvider([

'sql'=>"SELECT AVG(edad)media_Edad FROM ciclista GROUP BY nomequipo;",

'totalCount'=>$numero,
'pagination'=>[
'pageSize' => 5,
]
]);


    return $this->render ("resultados",[

        "resultado"=>$dataProvider,

        "campos"=>['media_Edad'],

        "titulo"=>"Consulta 5 con DAO",

        "enunciado"=>"La edad media de los ciclistas por cada equipo ",

        "sql"=>"SELECT AVG(edad) FROM ciclista GROUP BY nomequipo;",

]);
}
 public function actionConsulta6a(){
        //Mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find() ->select("count(nombre)numero_Total")->distinct()->groupBy("nomequipo"),
            'pagination'=>[
                    'pageSize'=>5,
                ]
            ]);
        
        return $this->render("resultados",[
            "resultado"=>$dataProvider,
            "campos"=>['numero_Total'],
            "titulo"=>"Consulta 6 con Active Record",
            "enunciado"=>"El número de ciclistas por equipo ",
            "sql"=>"SELECT count(nombre) FROM ciclista GROUP BY nomequipo;",
            
        ]);
    }
             /*CONSULTAS CON DAO*/
public function actionConsulta6(){

$numero = Yii::$app->db
	->createCommand("SELECT count(nombre) numero_Total FROM ciclista GROUP BY nomequipo;")
	->queryScalar();


$dataProvider = new SqlDataProvider([

'sql'=>"SELECT count(nombre) numero_Total FROM ciclista GROUP BY nomequipo;",

'totalCount'=>$numero,
'pagination'=>[
'pageSize' => 5,
]
]);


    return $this->render ("resultados",[

        "resultado"=>$dataProvider,

        "campos"=>['numero_Total'],

        "titulo"=>"Consulta 6 con DAO",

        "enunciado"=>"El número de ciclistas por equipo ",

        "sql"=>"SELECT count(nombre) numero_Total FROM ciclista GROUP BY nomequipo;",

]);
}
 public function actionConsulta7a(){
        //Mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Puerto::find() ->select("COUNT(*) total"),
            'pagination'=>[
                    'pageSize'=>5,
                ]
            ]);
        
        return $this->render("resultados",[
            "resultado"=>$dataProvider,
            "campos"=>['total'],
            "titulo"=>"Consulta 7 con Active Record",
            "enunciado"=>"El número de puetos ",
            "sql"=>"SELECT COUNT(*) num_total_puertos FROM puerto ;",
            
        ]);
    }
             /*CONSULTAS CON DAO*/
public function actionConsulta7(){

$numero = Yii::$app->db
	->createCommand("SELECT COUNT(*) total FROM puerto ;")
	->queryScalar();


$dataProvider = new SqlDataProvider([

'sql'=>"SELECT COUNT(*) total FROM puerto ;",

'totalCount'=>$numero,
'pagination'=>[
'pageSize' => 5,
]
]);


    return $this->render ("resultados",[

        "resultado"=>$dataProvider,

        "campos"=>['total'],

        "titulo"=>"Consulta 7 con DAO",

        "enunciado"=>"El numero de puertos",

        "sql"=>"SELECT COUNT(*) total FROM puerto ;",

]);

}
 public function actionConsulta8a(){
        //Mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Puerto::find() ->select("COUNT(*) total")->where("altura > 1500"),
            'pagination'=>[
                    'pageSize'=>5,
                ]
            ]);
        
        return $this->render("resultados",[
            "resultado"=>$dataProvider,
            "campos"=>['total'],
            "titulo"=>"Consulta 8 con Active Record",
            "enunciado"=>"El numero total de puertos mayores de 1500 ",
            "sql"=>"SELECT COUNT(*) num_total_puertos FROM puerto where altura > 1500 ;",
            
        ]);
    }
      /*CONSULTAS CON DAO*/
    public function actionConsulta8(){

$numero = Yii::$app->db
	->createCommand("SELECT COUNT(*) total FROM puerto where altura > 1500 ;")
	->queryScalar();


$dataProvider = new SqlDataProvider([

'sql'=>"SELECT COUNT(*) total FROM puerto where altura > 1500 ;",

'totalCount'=>$numero,
'pagination'=>[
'pageSize' => 5,
]
]);


    return $this->render ("resultados",[

        "resultado"=>$dataProvider,

        "campos"=>['total'],

        "titulo"=>"Consulta 8 con DAO",

        "enunciado"=>"El numero de puertos mayores de 1500",

        "sql"=>"SELECT COUNT(*) total FROM puerto where altura > 1500 ;",

]);

}
 public function actionConsulta9a(){
        //Mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find() ->select("nomequipo, COUNT(nombre) numero_ciclistas_equipo")->groupBy("nomequipo")->having(" count(nombre) > 4"),
            'pagination'=>[
                    'pageSize'=>5,
                ]
            ]);
        
        return $this->render("resultados",[
            "resultado"=>$dataProvider,
            "campos"=>['nomequipo'],
            "titulo"=>"Consulta 9 con Active Record",
            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas ",
            "sql"=>"SELECT nomequipo, count(nombre) numero_ciclistas_equipo FROM ciclista GROUP BY nomequipo HAVING count(nombre)>4;",
            
        ]);
    }
          /*CONSULTAS CON DAO*/
public function actionConsulta9(){

    $numero = Yii::$app->db
            ->createCommand("SELECT count(nomequipo), count(nombre) numero_ciclistas_equipo FROM ciclista GROUP BY nomequipo HAVING count(nombre)>4")
            ->queryScalar();


    $dataProvider = new SqlDataProvider([

    'sql'=>"SELECT nomequipo, count(nombre) numero_ciclistas_equipo FROM ciclista GROUP BY nomequipo HAVING count(nombre)>4",

    'totalCount'=>$numero,
    'pagination'=>[
    'pageSize' => 5,
    ]
    ]);


    return $this->render ("resultados",[

        "resultado"=>$dataProvider,

        "campos"=>['nomequipo','numero_ciclistas_equipo'],

        "titulo"=>"Consulta 9 con DAO",

        "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas",

        "sql"=>"SELECT nomequipo, count(nombre) numero_ciclistas_equipo FROM ciclista GROUP BY nomequipo HAVING count(nombre)>4;",

    ]);


}
  public function actionConsulta10a(){
        //Mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find() ->select("nomequipo, COUNT(nombre) numero_ciclistas_equipo")->where("edad BETWEEN 28 and 32")->groupBy("nomequipo")->having(" count(nombre) > 4"),
            'pagination'=>[
                    'pageSize'=>5,
                ]
            ]);
        
        return $this->render("resultados",[
            "resultado"=>$dataProvider,
            "campos"=>['nomequipo'],
            "titulo"=>"Consulta 10 con Active Record",
            "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad esté entre 28 y 32 ",
            "sql"=>"SELECT nomequipo, count(nombre) numero_ciclistas_equipo FROM ciclista GROUP BY nomequipo HAVING count(nombre)>4;",
            
        ]);
    }   
    
              /*CONSULTAS CON DAO*/
    public function actionConsulta10(){

$numero = Yii::$app->db
	->createCommand("SELECT count(nomequipo), count(nombre) numero_ciclistas_equipo FROM ciclista WHERE edad BETWEEN 28 and 32 GROUP BY nomequipo HAVING count(nombre) > 4;")
	->queryScalar();


$dataProvider = new SqlDataProvider([

'sql'=>"SELECT count(nomequipo), count(nombre) numero_ciclistas_equipo FROM ciclista WHERE edad BETWEEN 28 and 32 GROUP BY nomequipo HAVING count(nombre) > 4;",

'totalCount'=>$numero,
'pagination'=>[
'pageSize' => 5,
]
]);


    return $this->render ("resultados",[

        "resultado"=>$dataProvider,

        "campos"=>['numero_ciclistas_equipo'],

        "titulo"=>"Consulta 10 con DAO",

        "enunciado"=>"Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad esté entre 28 y 32",

        "sql"=>"SELECT count(nomequipo), count(nombre) numero_ciclistas_equipo FROM ciclista WHERE edad BETWEEN 28 and 32 GROUP BY nomequipo HAVING count(nombre) > 4;",

]);
 }
 
 
  public function actionConsulta11a(){
        //Mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => \app\models\Etapa::find() ->select(" nombre, count(numetapa) etapas_ganadas")->where("ciclista.dorsal=etapa.dorsal")->groupBy("nombre"),
            'pagination'=>[
            'pageSize'=>5,
                ]
            ]);
        
        return $this->render("resultados",[
            "resultado"=>$dataProvider,
            "campos"=>['etapas_ganadas'],
            "titulo"=>"Consulta 11 con Active Record",
            "enunciado"=>"Indícame el número de etapas que ha ganado cada uno de los ciclistas ",
            "sql"=>"SELECT DISTINCT nombre,COUNT(numetapa) AS etapas_ganadas FROM ciclista,etapa WHERE ciclista.dorsal=etapa.dorsal GROUP BY nombre;",
            
        ]);
    }  


  public function actionConsulta12a(){
        //Mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => \app\models\Etapa::find() ->select("dorsal")->groupBy("dorsal")->having(" count(*) > 1"),
            'pagination'=>[
                    'pageSize'=>5,
                ]
            ]);
        
        return $this->render("resultados",[
            "resultado"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 12 con Active Record",
            "enunciado"=>"Indícame el dorsal de los ciclistas que hayan ganado más de una etapa",
            "sql"=>"SELECT dorsal FROM etapa GROUP BY dorsal HAVING count(*) > 1;",
            
        ]);
    } 

              /*CONSULTAS CON DAO*/
    public function actionConsulta12(){

$numero = Yii::$app->db
	->createCommand("SELECT dorsal FROM etapa GROUP BY dorsal HAVING count(*) > 1")
	->queryScalar();


$dataProvider = new SqlDataProvider([

'sql'=>"SELECT dorsal FROM etapa GROUP BY dorsal HAVING count(*) > 1",

'totalCount'=>$numero,
'pagination'=>[
'pageSize' => 5,
]
]);


    return $this->render ("resultados",[

        "resultado"=>$dataProvider,

        "campos"=>['dorsal'],

        "titulo"=>"Consulta 12 con DAO",

        "enunciado"=>"Indícame el dorsal de los ciclistas que hayan ganado más de una etapa",

        "sql"=>"SELECT dorsal FROM etapa GROUP BY dorsal HAVING count(*) > 1",

]);
 }



    
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }
    
      
    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
